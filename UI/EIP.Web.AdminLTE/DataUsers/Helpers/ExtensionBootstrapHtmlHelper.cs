﻿using System.Text;
using System.Web.Mvc;
using EIP.Common.Core.Extensions;
using EIP.Common.Entities.Dtos;

namespace EIP.Web.AdminLTE.DataUsers.Helpers
{
    public static class ExtensionBootstrapHtmlHelper
    {
        /// <summary>
        /// 验证及是否具有清除按钮
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private static string BootstrapAttr(BaseBootstrapInput input)
        {
            StringBuilder required = new StringBuilder();
            if (input.Required)
            {
                required.Append(" required ");
                required.Append(string.Format(" data-fv-notempty-message='{0}'", input.NotemptyMessage));
                if (input.Remote)
                {
                    required.Append(string.Format(" data-fv-remote='true' "));
                    required.Append(string.Format(" data-fv-remote-url='{0}'", input.RemoteUrl));
                    required.Append(string.Format(" data-fv-remote-message='{0}'", input.RemoteMessage));
                }

                if (input.StringLength)
                {
                    required.Append(string.Format(" data-fv-stringlength='true' "));
                    required.Append(string.Format(" data-fv-stringlength-min='{0}'", input.StringLengthMin));
                    required.Append(string.Format(" data-fv-stringlength-max='{0}'", input.StringLengthMax));
                    required.Append(string.Format(" data-fv-stringlength-message='{0}'", input.StringLengthMessage));
                }

                if (!input.Pattern.IsNullOrEmpty())
                {
                    required.Append(string.Format(" pattern='{0}' ", input.Pattern));
                    required.Append(string.Format(" data-fv-regexp-message='{0}'", input.RegexpMessage));
                }
            }
            if (!input.ClearButton)
            {
                required.Append(" data-pure-clear-button ");
            }
            return required.ToString();
        }

        /// <summary>
        /// BootstrapText
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        public static MvcHtmlString BootstrapText(this HtmlHelper htmlHelper, BootstrapBoxInput input)
        {
            var html = string.Format(@"<input type='text' value='{0}' name='{1}' class='form-control {2}' {3} {4} placeholder='{5}' {6}>", input.Value, input.Name, input.Class, input.ClearButton ? "data-pure-clear-button" : "", input.HtmlAttributes, input.Placeholder, BootstrapAttr(input));
            return new MvcHtmlString(html);
        }

        /// <summary>
        /// BootstrapText
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        public static MvcHtmlString BootstrapTextArea(this HtmlHelper htmlHelper, BootstrapBoxInput input)
        {
            var html = string.Format(@"<textarea value='{0}' name='{1}' class='form-control {2}' {3} {4} placeholder='{5}' {6}></textarea>", input.Value, input.Name, input.Class, input.ClearButton ? "data-pure-clear-button" : "", input.HtmlAttributes, input.Placeholder, BootstrapAttr(input));
            return new MvcHtmlString(html);
        }
    }
}