﻿var currentindex = 1,
    timerID,
    submitButton = $("#submit"),
    $submitMsg = $("#submitMsg");
method = {
    //绑定事件
    bindEvent: function () {
        submitButton.click(function () {
            method.submit();
        });

        $("#validateCode").click(function () {
            method.reloadVerifyCode();
        });

        $("#userCode,#userPwd,#verify").bind("keypress", function (event) {
            if (event.keyCode == "13") {
                method.submit();
            }
        });
    },

    submit: function () {
        
        var loading = submitButton.button('loading');
        var code = $("#userCode").val();
        var pwd = $("#userPwd").val();
        var verify = $("#verify").val();
        if (code === "") {
            $("#userCode").focus();
            loading.button('reset');
            loading.dequeue();
            formMessage("请输入用户名", 'warning', 'warning');
            return;
        }
        if (pwd === "") {
            $("#userPwd").focus();
            loading.button('reset');
            loading.dequeue();
            formMessage("请输入密码", 'warning', 'warning');
            return;
        }
        if (verify === "") {
            loading.button('reset');
            loading.dequeue();
            $("#verify").focus();
            formMessage("请输入验证码", 'warning', 'warning');
            return;
        }
        formMessage("正在登录中,请稍等...", 'success', 'spinner fa-spin');
        $.post("/Account/Submit",
            {
                verify: verify,
                code: code,
                pwd: pwd,
                remberme: $("#remember").prop("checked"),
                returnUrl: $("#returnHidden").val()
            },
            function (data) {
                var resultType = data.ResultSign;
                var resultMsg = data.Message; //--------跳转地址
                $("#btnSubmit").removeAttr("disabled");
                if (resultType === 2) {
                    loading.button('reset');
                    loading.dequeue();
                    formMessage(resultMsg, 'danger', "ban");
                    method.reloadVerifyCode();
                } else {
                    formMessage('登录验证成功,正在跳转首页', 'success', 'check');
                    setInterval(Load(resultMsg), 1000);
                }
            }, "json").success(function () {//成功

            }).error(function () {//失败
                loading.button('reset');
                loading.dequeue();
                formMessage("登录失败,请稍后重试", 'danger', "ban");
                method.reloadVerifyCode();
            }).complete(function () {//完成
            });
    },

    //重新加载验证码
    reloadVerifyCode: function () {
        var img = document.getElementById("verifyCode");
        img.src = "/Account/GetValidateCode?random=" + Math.random();
    }
}

$(document).ready(function () {
    $("#userCode").focus();
    method.bindEvent();
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
});

//登录加载
function Load(url) {
    window.location.href = url;
    return false;
}
//提示信息
function formMessage(msg, type,icon) {
    $submitMsg.show().html('').attr("class", "callout callout-" + type);
    $submitMsg.append('<h5><i class="icon fa fa-' + icon + '"></i>' + msg + ' </h5>');
}

$(document).ajaxStart(function () {
    Pace.restart();
});