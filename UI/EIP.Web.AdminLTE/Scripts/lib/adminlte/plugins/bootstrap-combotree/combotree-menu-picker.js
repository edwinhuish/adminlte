﻿(function ($) {
    $.fn.combotreeMenuPicker = function (options) {
        var combotreeMenuMouseOver = false,
            combotreeMenuPopupr = null,
            combotreeMenuZNodes,
                combotreeMenuTreeObj,
                combotreeMenuElement,
                combotreeMenuUnload = true;
        var settings = $.extend({}, options);
        return this.each(function () {
            combotreeMenuElement = this;
            if (!settings.buttonOnly && $(this).data("combotreeMenuPicker") == undefined) {
                $this = $(this).addClass("form-control");
                $wraper = $("<div/>", { class: "input-group" });
                $this.wrap($wraper);
                $button = $("<span class=\"input-group-addon pointer\"><i class=\"fa fa-sitemap\"></i></span>");
                $this.after($button);
                (function (ele) {
                    $button.click(function () {
                        createUI(ele);
                        initCombotreeMenuData();
                    });
                })($this);

                $(this).data("combotreeMenuPicker", { attached: true });
            }

            function search(p) {
                if (combotreeMenuTreeObj) {
                    var rootNodes = combotreeMenuTreeObj.getNodes(),
                        allNodes = combotreeMenuTreeObj.transformToArray(rootNodes),
                        checkedNodes = combotreeMenuTreeObj.getCheckedNodes(true);
                    $.each(checkedNodes, function () {
                        combotreeMenuTreeObj.checkNode(this, false);
                    });
                    combotreeMenuTreeObj.cancelSelectedNode();
                    if (p) {
                        combotreeMenuTreeObj.hideNodes(allNodes);
                        var nodes = combotreeMenuTreeObj.getNodesByParamFuzzy('name', p),
                            parentNode,
                            o = {};
                        $.each(nodes, function (i, node) {
                            if (!node.isParent) {
                                parentNode = node.getParentNode();
                                o[node.tId] = node;
                                while (parentNode) {
                                    o[parentNode.tId] || (o[parentNode.tId] = parentNode);
                                    parentNode = parentNode.getParentNode();
                                }
                            }
                            return true;
                        });
                        $.each(o, function (key, value) {
                            combotreeMenuTreeObj.showNode(value);
                        });
                        $.each(rootNodes, function (i, node) {
                            node.isParent && o[node.tId] && combotreeMenuTreeObj.expandNode(node, true, true);
                        });
                    } else {
                        combotreeMenuTreeObj.showNodes(allNodes);
                        combotreeMenuTreeObj.expandAll(false);
                    }
                }
            }

            function createUI($element) {
                var editformOffset = $("#editform").offset();
                combotreeMenuPopupr = $('<div/>', {
                    css: {
                        'top': $element.offset().top + $element.outerHeight() - editformOffset.top + 6,
                        'left': $element.offset().left - editformOffset.left
                    },
                    class: 'combotree-popup'
                });
                combotreeMenuPopupr.html('<div class="ip-control"> \
						         <div class="box-tools"><div class="input-group" style="width: 272px;padding-left:5px"><input id="searchMenuKey" type="text" class="form-control input-sm" placeholder="请输入名称进行模糊查询..."><div class="input-group-btn"><a class="btn btn-sm btn-default"><i class="fa fa-search"></i></a></div></div></div></li> \
						         </div> \
						     <ul id="combotree" class="ztree" style="margin-top:0; height: 223px; overflow: auto"></ul> \
					         ').appendTo("#editform");
                combotreeMenuPopupr.addClass('dropdown-menu').show();
                combotreeMenuPopupr.mouseenter(function () { combotreeMenuMouseOver = true; }).mouseleave(function () { combotreeMenuMouseOver = false; });
                $(document).mouseup(function (e) {
                    if (!combotreeMenuPopupr.is(e.target) && combotreeMenuPopupr.has(e.target).length === 0) {
                        removeInstance();
                    }
                });
                $('#searchMenu', combotreeMenuPopupr).on("click", function (e) {
                    search($("#searchMenuKey").val());
                });

                $('#searchMenuKey', combotreeMenuPopupr).on("keyup", function (e) {
                    search($(this).val());
                });
              
            }
            function removeInstance() {
                $(".combotree-popup").remove();
            }
            //树点击触发
            function combotreeOnClickTree(e, treeId, treeNode) {
                $("#MenuId").val(treeNode.id);
                //加载数据
                $(combotreeMenuElement).val(treeNode.name);

            }
            //初始化模块
            function initCombotreeMenu() {
                //配置
                var setting = {
                    view: {
                        dblClickExpand: false,
                        showLine: true
                    },
                    data: {
                        simpleData: {
                            enable: true
                        }
                    },
                    callback: {
                        onClick: combotreeOnClickTree
                    }
                };
                combotreeMenuTreeObj = $.fn.zTree.init($("#combotree", combotreeMenuPopupr), setting, combotreeMenuZNodes);
            }



            //初始化树结构
            function initCombotreeMenuData() {
                if (combotreeMenuUnload) {
                    //判断是否具有筛选菜单条件
                    var menuType = UtilGetUrlParam("menuType");
                    var postUrl = "/Common/UserControl/GetMenuRemoveChildren";
                    //判断类型
                    switch (menuType) {
                        case Language.menuType.haveMenuPermission.toString():
                            break;
                        case Language.menuType.haveDataPermission.toString():
                            postUrl = "/System/Menu/GetHaveDataPermissionMenu";
                            break;
                        case Language.menuType.haveFieldPermission.toString():
                            postUrl = "/System/Menu/GetHaveFieldPermissionMenu";
                            break;
                        case Language.menuType.haveFunctionPermission.toString():
                            break;
                        case Language.menuType.isFreeze.toString():
                            break;
                        case Language.menuType.isShowMenu.toString():
                            break;
                        default:
                    }
                    UtilAjaxPostAsync(postUrl, {
                        menuId: UtilGetUrlParam("menuId"),
                        isRemove: UtilGetUrlParam("isRemove")
                    }, function (data) {
                        combotreeMenuZNodes = data;
                        initCombotreeMenu();
                        //绑定选中值
                        var pId = UtilGetUrlParam("parentId");
                        if (pId != "" && pId != null) {
                            ZtreeAssignCheck(treeObj, pId);
                        }
                        combotreeMenuUnload = false;
                    });
                } else {
                    initCombotreeMenu();
                }
            }
        });
    }

}(jQuery));
