(function ($) {

    $.fn.combotreePicker = function (options) {
        var combotreeMouseOver = false,
            combotreePopupr = null,
            combotreeZNodes,
                combotreeTreeObj,
                combotreeElement, combotreeUnload = true;
        var settings = $.extend({

        }, options);
        return this.each(function () {
            combotreeElement = this;
            if (!settings.buttonOnly && $(this).data("combotreePicker") == undefined) {
                $this = $(this).addClass("form-control");
                $wraper = $("<div/>", { class: "input-group" });
                $this.wrap($wraper);
                $button = $("<span class=\"input-group-addon pointer\"><i class=\"fa fa-sitemap\"></i></span>");
                $this.after($button);
                (function (ele) {
                    $button.click(function () {
                        createUI(ele);
                        initCombotreeData();
                    });
                })($this);

                $(this).data("combotreePicker", { attached: true });
            }

            function createUI($element) {
                combotreePopupr = $('<div/>', {
                    css: {
                        'top': $element.offset().top + $element.outerHeight() + 6,
                        'left': $element.offset().left
                    },
                    class: 'combotree-popup'
                });

                combotreePopupr.html('<ul id="combotree" class="ztree" style=" overflow: auto;height:265px"></ul>').appendTo("body");


                combotreePopupr.addClass('dropdown-menu').show();
                combotreePopupr.mouseenter(function () { combotreeMouseOver = true; }).mouseleave(function () { combotreeMouseOver = false; });

                $(document).mouseup(function (e) {
                    if (!combotreePopupr.is(e.target) && combotreePopupr.has(e.target).length === 0) {
                        removeInstance();
                    }
                });

            }
            function removeInstance() {
                $(".combotree-popup").remove();
            }
            //树点击触发
            function combotreeOnClickTree(e, treeId, treeNode) {
                $("#MenuId").val(treeNode.id);
                //加载数据
                $(combotreeElement).val(treeNode.name);
                
            }
            //初始化模块
            function initCombotree() {
                //配置
                var setting = {
                    view: {
                        dblClickExpand: false,
                        showLine: true
                    },
                    data: {
                        simpleData: {
                            enable: true
                        }
                    },
                    callback: {
                        onClick: combotreeOnClickTree
                    }
                };
                combotreeTreeObj = $.fn.zTree.init($("#combotree", combotreePopupr), setting, combotreeZNodes);
            }

            //初始化树结构
            function initCombotreeData() {
                if (combotreeUnload) {
                    UtilAjaxPost("/System/Menu/GetAllMenu", null, function(data) {
                        combotreeZNodes = data;
                        initCombotree();
                        combotreeUnload = false;
                    });
                } else {
                    initCombotree();
                }
            }
        });
    }

}(jQuery));
