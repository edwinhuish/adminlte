﻿using System;
using System.Web.Mvc;
using EIP.Common.Core.Utils;
using System.ComponentModel;
using System.Threading.Tasks;
using EIP.Common.Core.Auth;
using EIP.Common.Core.Log;
using EIP.Common.Entities;
using EIP.System.Business.Identity;
using EIP.System.Business.Log;
using EIP.System.Models.Dtos.Identity;
using SystemWeb = System.Web;
namespace EIP.Web.AdminLTE.Controllers
{
    public class AccountController : Controller
    {
        #region 构造函数

        private readonly ISystemUserInfoLogic _userInfoLogic;
        private readonly ISystemLoginLogLogic _loginLogLogic;

        public AccountController(ISystemUserInfoLogic userInfoLogic, ISystemLoginLogLogic loginLogLogic)
        {
            _userInfoLogic = userInfoLogic;
            _loginLogLogic = loginLogLogic;
        }

        #endregion

        #region 视图

        /// <summary>
        ///     登录
        /// </summary>
        /// <returns></returns>
        public ViewResultBase Login()
        {
            return View();
        }

        /// <summary>
        ///     生成验证码
        /// </summary>
        /// <returns></returns>
        public FileResult GetValidateCode()
        {
            var bytes = VerifyCodeUtil.GetVerifyCodeImage();
            return File(bytes, @"image/jpeg");
        }
        #endregion

        #region 方法

        /// <summary>
        ///     Post提交
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<JsonResult> Submit(UserLoginInput input)
        {
            try
            {
                var operateStatus = new OperateStatus();
                //获取生成验证码的结果值
                var verifyCode = VerifyCodeUtil.GetVerifyCode();
                //判断录入验证码和生成的验证码值是否相等
                if (input.Verify != verifyCode)
                {
                    operateStatus.ResultSign = ResultSign.Error;
                    operateStatus.Message = "验证码错误";
                    return Json(operateStatus);
                }
                //验证数据库信息
                var info = await _userInfoLogic.CheckUserByCodeAndPwd(input);
                if (info.Data != null)
                {
                    var principalUser = new PrincipalUser
                    {
                        UserId = info.Data.UserId,
                        Code = info.Data.Code,
                        Name = info.Data.Name,
                        OrganizationId = info.Data.OrganizationId,
                        OrganizationName = info.Data.OrganizationName
                    };
                    principalUser.LoginId = Guid.NewGuid();
                    //写入Cookie信息
                    FormAuthenticationExtension.SetAuthCookie(principalUser.UserId.ToString(), principalUser, input.Remberme);
                    //是否具有返回路径
                    if (Url.IsLocalUrl(input.ReturnUrl) && input.ReturnUrl.Length > 1 && input.ReturnUrl.StartsWith("/")
                        && !input.ReturnUrl.StartsWith("//") && !input.ReturnUrl.StartsWith("/\\"))
                    {
                        info.ResultSign = ResultSign.Successful;
                        info.Message = input.ReturnUrl;
                    }
                    //写入日志
                    WriteLoginLog(principalUser.LoginId);
                }
                return Json(info);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        ///     写入登录日志
        /// </summary>
        /// <param name="loginId">登录日志Id</param>
        private void WriteLoginLog(Guid loginId)
        {
            //获取当前用户信息
            var logHandler = new LoginLogHandler(loginId);
            logHandler.WriteLog();
        }

        /// <summary>
        ///     退出
        /// </summary>
        /// <returns></returns>
        [Description("退出系统")]
        public async Task<RedirectResult> Logout()
        {
            //获取当前用户信息
            var currentUser = FormAuthenticationExtension.Current(SystemWeb.HttpContext.Current.Request);
            if (currentUser != null)
            {
                var loginLog = await _loginLogLogic.GetByIdAsync(currentUser.LoginId);
                if (loginLog != null)
                {
                    loginLog.LoginOutTime = DateTime.Now;
                    var timeSpan = (TimeSpan)(loginLog.LoginOutTime - loginLog.LoginTime);
                    loginLog.StandingTime = timeSpan.TotalHours;
                    _loginLogLogic.UpdateAsync(loginLog);
                }
            }
            FormAuthenticationExtension.SignOut();
            return Redirect("/Account/Login");
        }

        #endregion
	}
}