﻿using System;

namespace EIP.Web.AdminLTE.Areas.System.Models
{
    /// <summary>
    /// 用户角色
    /// </summary>
    public class SystemUserRoleViewModel
    {
        public Guid R { get; set; }
    }
}