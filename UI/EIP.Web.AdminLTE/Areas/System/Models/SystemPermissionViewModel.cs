﻿using System;

namespace EIP.Web.AdminLTE.Areas.System.Models
{
    public class SystemPermissionViewModel
    {
        public Guid P { get; set; }
    }
}