﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using EIP.Common.Core.Attributes;
using EIP.Common.Core.Utils;
using EIP.Web.AdminLTE.Areas.System.Models;
using EIP.Common.Web;

namespace EIP.Web.AdminLTE.Areas.System.Controllers
{
    /// <summary>
    /// 运行时控制器
    /// </summary>
    public class RunningController : BaseController
    {
        #region 程序集
        /// <summary>
        /// 程序集
        /// </summary>
        /// <returns></returns>
        [CreateBy("孙泽伟")]
        [Description("程序集-视图-程序集列表")]
        public ViewResultBase AssemblyList()
        {
            return View();
        }

        /// <summary>
        /// 根据关键名称获取程序集信息
        /// </summary>
        /// <param name="fullName">关键字</param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Description("程序集-方法-根据关键名称获取程序集信息")]
        public JsonResult  GetAssemblyByFullName(string fullName = "")
        {
            IList<SystemRunningViewModel> assemblies = AssemblyUtil.GetAssemblyByFullName(fullName).Select(assembly => new SystemRunningViewModel
            {
                Name = assembly.GetName().Name,
                ClrVersion = assembly.ImageRuntimeVersion,
                Version = assembly.GetName().Version.ToString()
            }).ToList();
            return Json(assemblies.OrderBy(o => o.Name));
        }

        #endregion

        #region 数据库

        /// <summary>
        /// 数据库
        /// </summary>
        /// <returns></returns>
        [CreateBy("孙泽伟")]
        [Description("数据库-视图-列表")]
        public ViewResultBase DataBaseList()
        {
            return View();
        }

        #endregion

        #region 缓存

        /// <summary>
        /// 缓存
        /// </summary>
        /// <returns></returns>
        [CreateBy("孙泽伟")]
        [Description("缓存-视图-列表")]
        public ViewResultBase CacheList()
        {
            return View();
        }
        #endregion
    }
}
