﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Web.Mvc;
using EIP.Common.Core.Attributes;
using EIP.Common.Entities.Dtos;
using EIP.System.Business.Identity;
using EIP.System.Models.Entities;
using EIP.Common.Web;

namespace EIP.Web.AdminLTE.Areas.System.Controllers
{
    /// <summary>
    ///     岗位控制器
    /// </summary>
    public class PostController : BaseController
    {
        #region 构造函数
        private readonly ISystemPostLogic _postLogic;
        public PostController(ISystemPostLogic postLogic)
        {
            _postLogic = postLogic;
        }
        #endregion

        #region 视图
        /// <summary>
        ///     列表
        /// </summary>
        /// <returns></returns>
        [CreateBy("孙泽伟")]
        [Description("岗位维护-视图-列表")]
        public ViewResultBase List()
        {
            return View();
        }

        /// <summary>
        /// 岗位基础信息编辑
        /// </summary>
        /// <param name="organizationId">组织机构Id</param>
        /// <param name="postId">岗位Id</param>
        /// <returns></returns>
        [CreateBy("孙泽伟")]
        [Description("岗位维护-视图-编辑")]
        public async Task<ViewResultBase> Edit(Guid? postId = null,
            Guid? organizationId = null)
        {
            SystemPost post = new SystemPost();
            //如果为编辑
            if (postId != null)
            {
                post = await _postLogic.GetByIdAsync(postId);
            }
            //新增
            else
            {
                post.CreateTime = DateTime.Now;
                if (organizationId != null)
                    post.OrganizationId = (Guid)organizationId;
            }
            return View(post);
        }
        #endregion

        #region 方法
        /// <summary>
        /// 根据组织机构获取岗位信息
        /// </summary>
        /// <param name="input">组织机构Id</param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Description("岗位维护-方法-列表-根据组织机构获取岗位信息")]
        public async Task<JsonResult> GetPostByOrganizationId(NullableIdInput input)
        {
            return Json(await _postLogic.GetPostByOrganizationId(input));
        }

        /// <summary>
        /// 检测代码是否已经具有重复项
        /// </summary>
        /// <param name="input">需要验证的参数</param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Description("岗位维护-方法-新增/编辑-检测代码是否已经具有重复项")]
        public async Task<JsonResult> CheckPostCode(CheckSameValueInput input)
        {
            return JsonForCheckSameValue(await _postLogic.CheckPostCode(input));
        }

        /// <summary>
        ///     保存岗位数据
        /// </summary>
        /// <param name="post">岗位信息</param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Description("岗位维护-方法-新增/编辑-保存")]
        public async Task<JsonResult> SavePost(SystemPost post)
        {
            post.CreateUserId = CurrentUser.UserId;
            post.CreateUserName = CurrentUser.Name;
            return Json(await _postLogic.SavePost(post));
        }

        /// <summary>
        ///    删除岗位数据
        /// </summary>
        /// <param name="input">岗位Id</param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Description("岗位维护-方法-列表-删除")]
        public async Task<JsonResult> DeletePost(IdInput input)
        {
            return Json(await _postLogic.DeletePost(input));
        }
        #endregion
    }
}