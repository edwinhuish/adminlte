﻿using System.ComponentModel;
using System.Threading.Tasks;
using System.Web.Mvc;
using EIP.Common.Core.Attributes;
using EIP.Common.Entities.Dtos;
using EIP.System.Business.Identity;
using EIP.System.Models.Dtos.Identity;
using EIP.Common.Web;

namespace EIP.Web.AdminLTE.Areas.Console.Controllers
{
    public class MainController : BaseController
    {

        #region 构造函数

        private readonly ISystemUserInfoLogic _userInfoLogic;

        public MainController(ISystemUserInfoLogic userInfoLogic)
        {
            _userInfoLogic = userInfoLogic;
        }

        #endregion

        #region 视图
        /// <summary>
        ///     修改密码
        /// </summary>
        /// <returns></returns>
        [CreateBy("孙泽伟")]
        [Description("主界面-视图-修改密码")]
        public ViewResultBase ChangePassword()
        {
            return View();
        }

        #endregion

        #region 方法

        /// <summary>
        ///     保存修改密码
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Description("主界面-方法-保存修改后密码信息")]
        public async Task<JsonResult> SaveChangePassword(ChangePasswordInput input)
        {
            input.Id = CurrentUser.UserId;
            return Json(await _userInfoLogic.SaveChangePassword(input));
        }

        /// <summary>
        ///     验证旧密码是否输入正确
        /// </summary>
        /// <param name="input">需要验证的参数</param>
        /// <returns></returns>
        [CreateBy("孙泽伟")]
        [Description("主界面-方法-验证旧密码是否输入正确")]
        public async Task<JsonResult> CheckOldPassword(CheckSameValueInput input)
        {
            input.Id = CurrentUser.UserId;
            return JsonForCheckSameValue(await _userInfoLogic.CheckOldPassword(input));
        }
        #endregion
    }
}