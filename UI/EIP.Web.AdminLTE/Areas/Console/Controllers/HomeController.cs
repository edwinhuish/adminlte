﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Web.Mvc;
using EIP.Common.Core.Attributes;
using EIP.System.Business.Permission;
using EIP.Common.Web;
using System.ComponentModel;
using System.Linq;
using System.Text;
using EIP.Common.Core.Extensions;
using EIP.Common.Entities.Tree;

namespace EIP.Web.AdminLTE.Areas.Console.Controllers
{
    public class HomeController : BaseController
    {
        #region 视图

        /// <summary>
        ///     首页
        /// </summary>
        /// <returns></returns>
        [CreateBy("孙泽伟")]
        [Description("首页-界面")]
        [Ignore]
        public ViewResultBase Index()
        {
            return View();
        }

        #endregion

        #region 方法

        private StringBuilder _permissionStr = new StringBuilder();
        /// <summary>
        ///     加载模块权限
        /// </summary>
        /// <returns></returns>
        [CreateBy("孙泽伟")]
        [Description("首页-方法-获取菜单模块权限")]
        [Ignore]
        public async Task<string> LoadMenuPermission()
        {
            //所有权限
            var permissions = (await _permissionLogic.GetSystemPermissionMenuByUserId(CurrentUser.UserId)).ToList();
            //所有第一级
            var firstPermissions = permissions.Where(f => f.pId.ToString() == Guid.Empty.ToString()).ToList();
            foreach (var permission in firstPermissions)
            {
                _permissionStr.Append(GetWdChildNodes(permissions, permission));
            }
            return _permissionStr.ToString();
        }

        /// <summary>
        ///     根据当前节点，加载子节点
        /// </summary>
        /// <param name="treeEntitys">TreeEntity的集合</param>
        /// <param name="currTreeEntity">当前节点</param>
        private string GetWdChildNodes(IList<TreeEntity> treeEntitys,
            TreeEntity currTreeEntity)
        {
            StringBuilder childTrees = new StringBuilder();
            //当前节点是否还有下级
            IList<TreeEntity> childNodes = treeEntitys.Where(f => f.pId.ToString() == currTreeEntity.id.ToString()).ToList();
            if (childNodes.Count <= 0)
            {
                childTrees.Append(string.Format(@" <li>
                            <a href='{0}'><i class='fa {1}'></i>{2}</a>
                        </li>", currTreeEntity.url.IsNullOrEmpty() ? "#" : currTreeEntity.url, currTreeEntity.icon.IsNullOrEmpty() ? "fa-circle-o" : currTreeEntity.icon, currTreeEntity.name)).ToString();
            }
            else
            {
                childTrees.Append(string.Format(@"<li {3}>
                            <a href='{0}'>
                                <i class='fa {1}'></i><span>{2}</span> 
                                <span class='pull-right-container'>
                                    <i class='fa fa-angle-left pull-right'></i>
                                </span>
                            </a><ul class='treeview-menu'>"
                    , currTreeEntity.url.IsNullOrEmpty() ? "#" : currTreeEntity.url, currTreeEntity.icon.IsNullOrEmpty() ? "fa-tasks" : currTreeEntity.icon, currTreeEntity.name, currTreeEntity.pId.ToString() == Guid.Empty.ToString() ? "class='treeview'" : ""));
                //下面还有值
                foreach (var treeEntity in childNodes)
                {
                    childTrees.Append(GetWdChildNodes(treeEntitys, treeEntity));
                }
                childTrees.Append("</ul></li>");
            }
            return childTrees.ToString();
        }

        /// <summary>
        ///     替换模板中的字段值
        /// </summary>
        public string ReplaceText(string userName, string name, string myName)
        {
            var path = Server.MapPath("\\Templates\\Email\\TestTemplate.html");

            if (path == string.Empty)
            {
                return string.Empty;
            }
            var sr = new StreamReader(path);
            var str = sr.ReadToEnd();
            str = str.Replace("$USER_NAME$", userName);
            str = str.Replace("$NAME$", name);
            str = str.Replace("$MY_NAME$", myName);

            return str;
        }

        #endregion

        #region 构造函数

        private readonly ISystemPermissionLogic _permissionLogic;

        public HomeController(ISystemPermissionLogic permissionLogic)
        {
            _permissionLogic = permissionLogic;
        }

        #endregion
    }
}