﻿using System;

namespace EIP.Common.Entities.CustomAttributes
{
    /// <summary>
    /// 主键信息
    /// </summary>
    public class IdInfo
    {
        /// <summary>
        /// Id名称
        /// </summary>
        public String Key { get; set; }
        /// <summary>
        /// Id值
        /// </summary>
        public Object Value { get; set; }
    }
}