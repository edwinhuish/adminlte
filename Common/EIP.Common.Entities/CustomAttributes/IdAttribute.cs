﻿using System;

namespace EIP.Common.Entities.CustomAttributes
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class IdAttribute : Attribute
    {
        private string _name = string.Empty;
        private int _strategy = GenerationType.Indentity;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public int Strategy
        {
            get { return _strategy; }
            set { _strategy = value; }
        }
    }
}