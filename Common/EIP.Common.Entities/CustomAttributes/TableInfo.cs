﻿namespace EIP.Common.Entities.CustomAttributes
{
    /// <summary>
    /// 表基础信息
    /// </summary>
    public class TableInfo
    {
        /// <summary>
        /// 列信息
        /// </summary>
        private ColumnInfo _columns = new ColumnInfo();
        /// <summary>
        /// 主键信息
        /// </summary>
        private IdInfo _id = new IdInfo();
        /// <summary>
        /// 
        /// </summary>
        private Map _propToColumn = new Map();
        /// <summary>
        /// 表名称
        /// </summary>
        public string TableName { get; set; }
        /// <summary>
        /// 策略
        /// </summary>
        public int Strategy { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public string Sort { get; set; }
        
        public IdInfo Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public ColumnInfo Columns
        {
            get { return _columns; }
            set { _columns = value; }
        }

        public Map PropToColumn
        {
            get { return _propToColumn; }
            set { _propToColumn = value; }
        }
    }
}