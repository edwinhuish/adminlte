﻿using System;

namespace EIP.Common.Entities.CustomAttributes
{
    /// <summary>
    /// 缓存处理模式
    /// </summary>
    public enum CacheMode
    {
        /// <summary>
        /// 全表缓存
        /// </summary>
        AllValue = 0
    }
    /// <summary>
    /// 需要缓存时配置的特性
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public class NeedCacheAttribute : Attribute
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public NeedCacheAttribute(): this(CacheMode.AllValue){}

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="cacheMode">缓存模式</param>
        /// <param name="expiry">过期时间</param>
        public NeedCacheAttribute(CacheMode cacheMode, TimeSpan expiry){}

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="cache">缓存模式</param>
        public NeedCacheAttribute(CacheMode cache): this(cache, TimeSpan.Zero){}

        /// <summary>
        /// 缓存处理模式
        /// </summary>
        public CacheMode CacheMode { get; set; }

        /// <summary>
        /// 有效期
        /// </summary>
        public TimeSpan Expiry { get; set; }
    }
}