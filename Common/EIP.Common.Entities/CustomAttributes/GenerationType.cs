﻿namespace EIP.Common.Entities.CustomAttributes
{
    public class GenerationType
    {
        public const int Indentity = 1; //自动增长
        public const int Sequence = 2; //序列
        public const int Table = 3; //TABLE

        private GenerationType()
        {
        }
    }
}