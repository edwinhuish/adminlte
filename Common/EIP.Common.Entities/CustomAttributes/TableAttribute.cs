﻿using System;

namespace EIP.Common.Entities.CustomAttributes
{
    /// <summary>
    /// 表特性
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public class TableAttribute : Attribute
    {
        private string _name = string.Empty;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
    }
}