﻿using System;

namespace EIP.Common.Entities.CustomAttributes
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class ColumnAttribute : Attribute
    {
        private bool _isInsert = true; //是否插入到表中
        private bool _isNull = true; //是否允许为空
        private bool _isSelect = true; //是否查询
        private bool _isUpdate = true; //是否修改到表中
        private string _name = string.Empty; //列名        
        private string _sortType = "Desc";//默认降序排序
        private string _description = string.Empty;//描述
        public ColumnAttribute()
        {
            IsUnique = false;
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public string SortType
        {
            get { return _sortType; }
            set { _sortType = value; }
        }
        public bool IsUnique { get; set; }

        public bool IsNull
        {
            get { return _isNull; }
            set { _isNull = value; }
        }

        public bool IsInsert
        {
            get { return _isInsert; }
            set { _isInsert = value; }
        }

        public bool IsUpdate
        {
            get { return _isUpdate; }
            set { _isUpdate = value; }
        }

        public bool IsSelect
        {
            get { return _isSelect; }
            set { _isSelect = value; }
        }

        public bool IsSort { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
    }
}