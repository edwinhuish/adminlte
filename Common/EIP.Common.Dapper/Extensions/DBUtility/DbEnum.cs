﻿namespace EIP.Common.Dapper.Extensions.DBUtility
{
    /// <summary>
    ///     数据库类型枚举，需要扩展类型可在此添加
    /// </summary>
    public enum DatabaseType
    {
        Sqlserver,
        Oracle,
        Access,
        Mysql
    }

    /// <summary>
    ///     对数据库操作枚举
    /// </summary>
    public enum DbOperateType
    {
        Insert,
        Update,
        Delete,
        Select
    }
}