﻿//using System.Collections.Generic;
//using System.Linq;
//using EIP.Common.Core.Log;
//using EIP.Common.Core.Utils;
//using EIP.Common.Dapper;
//using EIP.Common.Dapper.AdoNet;
//using EIP.Common.Dapper.Extensions.Common;
//using EIP.Common.Dapper.Extensions.DBUtility;
//using EIP.Common.Entities.CustomAttributes;
//using EIP.Common.Entities.Paging;
//using Newtonsoft.Json;

//namespace EIP.Common.DataAccess
//{
//    /// <summary>
//    ///     DapperRepository仓储,T代表实体信息,规范约束为T必须继承IEntityBase接口
//    /// </summary>
//    /// <typeparam name="T">实体</typeparam>
//    public class DapperRepository<T> : BaseRepository, IRepository<T> where T : class, new()
//    {
//        #region 修改

//        /// <summary>
//        ///     更新
//        /// </summary>
//        /// <param name="current">实体信息</param>
//        /// <returns>影响条数</returns>
//        public virtual int Update(T current)
//        {
//            //查询缓存中的Sql语句
//            var tableInfo = DbEntityUtils.GetTableInfo(current, DbOperateType.Update);
//            LogWriter.WriteLog(FolderName.DataLog, string.Format("修改表{0}数据<br>修改前:{1}<br>修改后:{2}",
//                tableInfo.TableName,
//                JsonConvert.SerializeObject(GetByIdFromDataBase(current)),
//                JsonConvert.SerializeObject(current)));
//            var strSql = DbEntityUtils.GetUpdateSql(tableInfo);
//            var num = SqlMapperUtil.InsertUpdateOrDeleteSql(strSql, current);
//            //if (num > 0)
//            //    RefreshCacheForUpdate(current);
//            return num;
//        }

//        #endregion

//        #region 增加

//        /// <summary>
//        ///     插入
//        /// </summary>
//        /// <param name="entity">实体信息</param>
//        /// <returns></returns>
//        public virtual int Insert(T entity)
//        {
//            //查询缓存中的Sql语句
//            var tableInfo = DbEntityUtils.GetTableInfo(entity, DbOperateType.Insert);
//            LogWriter.WriteLog(FolderName.DataLog,
//                string.Format("新增表{0}数据<br>{1}", tableInfo.TableName, JsonConvert.SerializeObject(entity)));
//            var strSql = DbEntityUtils.GetInsertSql(tableInfo);
//            var num = SqlMapperUtil.InsertUpdateOrDeleteSql(strSql, entity);
//            //if (num > 0)
//            //    RefreshCacheForUpdate(entity);
//            return num;
//        }

//        /// <summary>
//        ///     批量插入
//        /// </summary>
//        /// <typeparam name="T">实体信息</typeparam>
//        /// <param name="list">实体集合</param>
//        /// <returns>影响条数</returns>
//        public virtual int InsertMultipleDapper(IEnumerable<T> list)
//        {
//            //查询缓存中的Sql语句
//            var tableInfo = DbEntityUtils.GetTableInfo(new T(), DbOperateType.Insert);
//            LogWriter.WriteLog(FolderName.DataLog,
//                string.Format("Dapper新增表{0}数据<br>{1}", tableInfo.TableName, JsonConvert.SerializeObject(list)));
//            var strSql = DbEntityUtils.GetInsertSql(tableInfo);
//            var num = SqlMapperUtil.InsertUpdateOrDeleteSql(strSql, list);
//            //if (num > 0)
//            //{
//            //    foreach (var l in list)
//            //    {
//            //        RefreshCacheForUpdate(l);
//            //    }
//            //}
//            return num;
//        }

//        /// <summary>
//        ///     批量插入
//        /// </summary>
//        /// <typeparam name="T">实体信息</typeparam>
//        /// <param name="list">实体集合</param>
//        /// <returns>影响条数</returns>
//        public virtual int InsertMultiple(IEnumerable<T> list)
//        {
//            var tableInfo = DbEntityUtils.GetTableInfo(new T(), DbOperateType.Insert);
//            LogWriter.WriteLog(FolderName.DataLog,
//                string.Format("批量新增表{0}数据<br>{1}", tableInfo.TableName, JsonConvert.SerializeObject(list)));
//            //查询缓存中的Sql语句
//            var num = AdoUtil.InsertList(list.ToList());
//            //if (num > 0)
//            //{
//            //    foreach (var l in list)
//            //    {
//            //        RefreshCacheForUpdate(l);
//            //    }
//            //}
//            return num;
//        }

//        #endregion

//        #region 删除

//        /// <summary>
//        ///     根据实体信息删除
//        /// </summary>
//        /// <param name="entity">实体信息</param>
//        /// <returns>影响条数</returns>
//        public virtual int Delete(T entity)
//        {
//            //查询缓存中的Sql语句
//            var tableInfo = DbEntityUtils.GetTableInfo(entity, DbOperateType.Delete);
//            LogWriter.WriteLog(FolderName.DataLog,
//                string.Format("删除表{0}数据<br>{1}", tableInfo.TableName, JsonConvert.SerializeObject(entity)));
//            var strSql = DbEntityUtils.GetDeleteByPropertySql(tableInfo);
//            var num = SqlMapperUtil.InsertUpdateOrDeleteSql(strSql, entity);
//            //if (num > 0)
//            //    RefreshCacheForDelete(entity);
//            return SqlMapperUtil.InsertUpdateOrDeleteSql(strSql, entity);
//        }

//        /// <summary>
//        ///     根据Id删除
//        /// </summary>
//        /// <param name="id">主键Id</param>
//        /// <returns>影响条数</returns>
//        public virtual int Delete(object id)
//        {
//            //查询缓存中的Sql语句
//            var tableInfo = DbEntityUtils.GetTableInfo(new T(), DbOperateType.Delete);
//            LogWriter.WriteLog(FolderName.DataLog,
//                string.Format("删除表{0}数据<br>{1}", tableInfo.TableName, JsonConvert.SerializeObject(GetById(id))));
//            var strSql = DbEntityUtils.GetDeleteByIdSql(tableInfo);
//            var num = SqlMapperUtil.InsertUpdateOrDeleteSql(strSql, new {Id = id});
//            //if (num > 0)
//            //    RefreshCacheForDelete(id);
//            return num;
//        }

//        /// <summary>
//        ///     批量删除
//        /// </summary>
//        /// <param name="ids"></param>
//        /// <returns></returns>
//        public virtual int DeleteBatch(string ids)
//        {
//            //查询缓存中的Sql语句
//            var tableInfo = DbEntityUtils.GetTableInfo(new T(), DbOperateType.Delete);
//            var strSql = DbEntityUtils.GetDeleteBatchSql(tableInfo, ids);
//            var num = SqlMapperUtil.InsertUpdateOrDeleteSql(strSql);
//            //if (num > 0)
//            //{
//            //    foreach (var l in ids.Split(','))
//            //    {
//            //        RefreshCacheForDelete(l);
//            //    }
//            //}
//            return num;
//        }

//        /// <summary>
//        /// 删除所有
//        /// </summary>
//        /// <returns></returns>
//        public virtual int DeleteAll()
//        {
//            //查询缓存中的Sql语句
//            var tableInfo = DbEntityUtils.GetTableInfo(new T(), DbOperateType.Delete);
//            var strSql = DbEntityUtils.DeleteAll(tableInfo);
//            return SqlMapperUtil.InsertUpdateOrDeleteSql(strSql);
//        }

//        #endregion

//        #region 查询

//        /// <summary>
//        ///     获取所有信息
//        /// </summary>
//        /// <returns></returns>
//        public virtual IEnumerable<T> GetAllEnumerable()
//        {
//            //查询缓存中的Sql语句
//            var tableInfo = DbEntityUtils.GetTableInfo(new T(), DbOperateType.Select);
//            var strSql = DbEntityUtils.GetFindAllSql(tableInfo);
//            var enumerable = SqlMapperUtil.SqlWithParamsDapper<T>(strSql, null);
//            ////刷新缓存
//            //if (enumerable.Any())
//            //{
//            //    RefreshCache(enumerable.ToList());
//            //}
//            return enumerable;
//        }

//        /// <summary>
//        ///     从数据库获取数据
//        /// </summary>
//        /// <param name="id"></param>
//        /// <returns></returns>
//        public virtual T GetByIdFromDataBase(object id)
//        {
//            //查询缓存中的Sql语句
//            var tableInfo = DbEntityUtils.GetTableInfo(new T(), DbOperateType.Select);
//            var strSql = DbEntityUtils.GetFindByIdSql(tableInfo);
//            return SqlMapperUtil.SqlWithParamsSingle<T>(strSql, new {Id = id});
//        }

//        /// <summary>
//        ///     从数据库获取数据
//        /// </summary>
//        /// <param name="current"></param>
//        /// <returns></returns>
//        public virtual T GetByIdFromDataBase(T current)
//        {
//            //查询缓存中的Sql语句
//            var tableInfo = DbEntityUtils.GetTableInfo(new T(), DbOperateType.Select);
//            var strSql = DbEntityUtils.GetFindByIdSql(tableInfo);
//            return SqlMapperUtil.SqlWithParamsSingle<T>(strSql, new
//            {
//                Id = AssemblyUtil.GetObjectPropertyValue(current, tableInfo.Id.Key)
//            });
//        }

//        /// <summary>
//        ///     根据主句Id获取实体信息
//        /// </summary>
//        /// <param name="id"></param>
//        /// <returns></returns>
//        public virtual T GetById(object id)
//        {
//            //查询缓存中的Sql语句
//            //var result = TryGetByIdFromCache(id);
//            //if (result != null)
//            //{
//            //    return result;
//            //}
//            var result = GetByIdFromDataBase(id);
//            ////如果不为空
//            //if (result != null)
//            //{
//            //    //刷新单值缓存
//            //    RefreshCache(id, result);
//            //}
//            return result;
//        }

//        /// <summary>
//        ///     复杂查询分页
//        /// </summary>
//        /// <typeparam name="T">类型</typeparam>
//        /// <param name="querySql">查询语句</param>
//        /// <param name="queryParam">查询参数</param>
//        /// <returns>分页结果</returns>
//        /// <remarks>
//        ///     注意事项：
//        ///     1.sql语句中需要加上@where、@orderBy、@rowNumber、@recordCount标记
//        ///     如: "select *, @rowNumber, @recordCount from ADM_Rule @where"
//        ///     2.实体中需增加扩展属性，作记录总数输出：RecordCount
//        ///     3.标记解释:
//        ///     @where：      查询条件
//        ///     @orderBy：    排序
//        ///     @x：          分页记录起点
//        ///     @y：          分页记录终点
//        ///     @recordCount：记录总数
//        ///     @rowNumber：  行号
//        ///     4.示例参考:
//        /// </remarks>
//        public PagedResults<T> PagingQuery<T>(string querySql, QueryParam queryParam)
//        {
//            var sql = queryParam.IsReport ? 
//                string.Format(@"select * from ({0}) seq ", querySql) :
//                string.Format(@"select * from ({0}) seq where seq.rownum between @x and @y", querySql);
//            var currentPage = queryParam.Page; //当前页号
//            var pageSize = queryParam.Rows; //每页记录数
//            var lower = ((currentPage - 1)*pageSize) + 1; //记录起点
//            var upper = currentPage*pageSize; //记录终点
//            var where = @" where 1=1 ";
//            if (!string.IsNullOrEmpty(queryParam._filters))
//            {
//                where += queryParam.Filters;
//            }
//            var parms = new DynamicParameters();
//            parms.Add("x", lower);
//            parms.Add("y", upper);

//            //排序字段
//            var orderString = string.Format("{0} {1}", queryParam.Sidx, queryParam.Sord);

//            sql = sql.Replace("@recordCount", " count(*) over() as RecordCount ")
//                .Replace("@rowNumber", " row_number() over (order by @orderBy) as rownum ")
//                .Replace("@orderBy", orderString)
//                .Replace("@where", where);
            
//            var data = SqlMapperUtil.SqlWithParamsDapper<T>(sql, parms).ToList();
//            var pagerInfo = new PagerInfo();
//            var first = data.FirstOrDefault();
//            //记录总数
//            if (first != null)
//                pagerInfo.RecordCount = (int) first.GetType().GetProperty("RecordCount").GetValue(first, null);
//            pagerInfo.Page = queryParam.Page;
//            pagerInfo.PageCount = (pagerInfo.RecordCount + queryParam.Rows - 1)/queryParam.Rows; //页总数 
//            var pagedResult = new PagedResults<T> {Data = data, PagerInfo = pagerInfo};

//            return pagedResult;
//        }

//        #endregion

//        #region 缓存

//        /// <summary>
//        ///     获取是否需要缓存特性
//        /// </summary>
//        private NeedCacheAttribute CacheAttribute
//        {
//            get
//            {
//                var entityType = typeof (T);
//                var attributes = entityType.GetCustomAttributes(typeof (NeedCacheAttribute), false);
//                if (attributes.Length == 1)
//                {
//                    return (NeedCacheAttribute) attributes[0];
//                }
//                return null;
//            }
//        }

//        /// <summary>
//        ///     是否需要缓存
//        /// </summary>
//        private bool NeedCache
//        {
//            get { return CacheAttribute != null && CacheAttribute.CacheMode == CacheMode.AllValue; }
//        }

//        ///// <summary>
//        /////     尝试从缓存中获取实体信息
//        ///// </summary>
//        ///// <param name="id"></param>
//        ///// <returns></returns>
//        //private T TryGetByIdFromCache(object id)
//        //{
//        //    //如果不需要缓存
//        //    if (!NeedCache)
//        //    {
//        //        return null;
//        //    }
//        //    var hashOperator = new RedisHashOperator();
//        //    var t = hashOperator.Get<T>(RecordCacheKey(id));
//        //    hashOperator.Dispose();
//        //    return t;
//        //}

//        ///// <summary>
//        /////     刷新整个表缓存
//        ///// </summary>
//        //private void RefreshCache(IList<T> objectSet)
//        //{
//        //    if (!NeedCache) return;
//        //    var hashOperator = new RedisHashOperator();

//        //    //清除所有单值缓存
//        //    if (objectSet.Count > 0)
//        //    {
//        //        foreach (var record in objectSet)
//        //        {
//        //            object id = DbEntityUtils.GetTableKeyValue(record);
//        //            hashOperator.Remove(InstanceCacheKey(), id.ToString());
//        //        }
//        //        foreach (var record in objectSet)
//        //        {
//        //            object id = DbEntityUtils.GetTableKeyValue(record);
//        //            hashOperator.Set(InstanceCacheKey(), id.ToString(), record);
//        //        }
//        //    }
//        //    hashOperator.Dispose();
//        //}

//        ///// <summary>
//        /////     刷新单值缓存
//        ///// </summary>
//        ///// <param name="id">主键Id</param>
//        ///// <param name="t">实体</param>
//        //private void RefreshCache(object id, T t)
//        //{
//        //    if (!NeedCache) return;
//        //    var hashOperator = new RedisHashOperator();
//        //    hashOperator.RemoveHash(RecordCacheKey(id));
//        //    hashOperator.Set(RecordCacheKey(id), t);
//        //    hashOperator.Dispose();
//        //}

//        ///// <summary>
//        /////     刷新更新单值缓存
//        ///// </summary>
//        ///// <param name="entity">实体</param>
//        //private void RefreshCacheForUpdate(T entity)
//        //{
//        //    if (!NeedCache) return;
//        //    var hashOperator = new RedisHashOperator();
//        //    object id = DbEntityUtils.GetTableKeyValue(entity);
//        //    //删除该缓存
//        //    hashOperator.RemoveHash(RecordCacheKey(id));
//        //    //把该实体放入缓存
//        //    hashOperator.Set(RecordCacheKey(id), entity);
//        //    hashOperator.Dispose();
//        //}

//        ///// <summary>
//        /////     刷新删除单值缓存
//        ///// </summary>
//        ///// <param name="id"></param>
//        //private void RefreshCacheForDelete(object id)
//        //{
//        //    if (!NeedCache) return;
//        //    var hashOperator = new RedisHashOperator();
//        //    //删除单值缓存
//        //    hashOperator.RemoveHash(RecordCacheKey(id));
//        //    hashOperator.Dispose();
//        //}

//        ///// <summary>
//        /////     刷新删除单值缓存
//        ///// </summary>
//        ///// <param name="entity"></param>
//        //private void RefreshCacheForDelete(T entity)
//        //{
//        //    if (!NeedCache) return;
//        //    var hashOperator = new RedisHashOperator();
//        //    //删除单值缓存
//        //    hashOperator.RemoveHash(RecordCacheKey(DbEntityUtils.GetTableKeyValue(entity)));
//        //    hashOperator.Dispose();
//        //}

//        /// <summary>
//        ///     实例缓存键值
//        /// </summary>
//        /// <returns></returns>
//        protected static string InstanceCacheKey()
//        {
//            return string.Format("{0}", typeof (T).Name);
//        }

//        /// <summary>
//        ///     单记录缓存键值
//        /// </summary>
//        /// <param name="id"></param>
//        /// <returns></returns>
//        protected static string RecordCacheKey(object id)
//        {
//            return string.Format("{0}_{1}", typeof (T).Name, id);
//        }

//        #endregion
//    }
//}