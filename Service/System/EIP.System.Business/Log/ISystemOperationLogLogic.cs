using EIP.Common.Business;
using EIP.System.Models.Entities;

namespace EIP.System.Business.Log
{
    public interface ISystemOperationLogLogic : IAsyncLogic<SystemOperationLog>
    {
    }
}