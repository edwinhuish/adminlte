using EIP.Common.Business;
using EIP.System.DataAccess.Log;
using EIP.System.Models.Entities;

namespace EIP.System.Business.Log
{
    public class SystemExceptionLogLogic : AsyncLogic<SystemExceptionLog>, ISystemExceptionLogLogic
    {
        #region ���캯��

        private readonly ISystemExceptionLogRepository _exceptionLogRepository;

        public SystemExceptionLogLogic(ISystemExceptionLogRepository exceptionLogRepository)
            : base(exceptionLogRepository)
        {
            _exceptionLogRepository = exceptionLogRepository;
        }

        #endregion
    }
}