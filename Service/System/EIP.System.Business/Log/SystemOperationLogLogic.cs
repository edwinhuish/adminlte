using EIP.Common.Business;
using EIP.System.DataAccess.Log;
using EIP.System.Models.Entities;

namespace EIP.System.Business.Log
{
    public class SystemOperationLogLogic : AsyncLogic<SystemOperationLog>, ISystemOperationLogLogic
    {
        #region ���캯��

        private readonly ISystemOperationLogRepository _repository;

        public SystemOperationLogLogic(ISystemOperationLogRepository operationLogRepository)
            : base(operationLogRepository)
        {
            _repository = operationLogRepository;
        }

        #endregion
    }
}