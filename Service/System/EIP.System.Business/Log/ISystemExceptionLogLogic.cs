using EIP.Common.Business;
using EIP.System.Models.Entities;

namespace EIP.System.Business.Log
{
    public interface ISystemExceptionLogLogic : IAsyncLogic<SystemExceptionLog>
    {
       
    }
}